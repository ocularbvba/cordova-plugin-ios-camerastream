# cordova-plugin-ios-camerastream
Plugin made by combining multiple other plugins. Made for iOS because it doesn't support webRTC in wkWebview (yet).

##(Re)start the stream (camera, fps)
Start the "back" or "front" camera with fps

```cordova.iosStream.startCapture('back', 30);```

##Pause the current stream
Pauses the current stream

```cordova.iosStream.pause();```

##Process capture data
Returns the stream as base64

```
cordova.iosStream.capture = function(data){
    //do something here for example:
    image.src = data;
}
```