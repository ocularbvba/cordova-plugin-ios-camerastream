import WebKit
import Foundation
import AVFoundation

@available(iOS 10.0, *)
@objc(IosStream)
class IosStream: CDVPlugin, AVCaptureVideoDataOutputSampleBufferDelegate {
    var session: AVCaptureSession?
    var mainCommand: CDVInvokedUrlCommand?
    var hasRun: Bool = false
    
    @objc(startCapture:) 
    func startCapture(command: CDVInvokedUrlCommand) {
        if(hasRun){
            if session?.isRunning ?? false {
                return
            }
            session?.startRunning()
            return
        }
        let cameraString = command.arguments[0] as? String ?? "front"
        let fps = command.arguments[1] as? Int32 ?? 30
        var camera: AVCaptureDevice
        
        switch cameraString {
        case "back":
            camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)!
        default:
            camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)!
        }
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        var videoDataOutput: AVCaptureVideoDataOutput!
        
        session = AVCaptureSession()
        session?.sessionPreset = AVCaptureSession.Preset.medium
        
        do{
            input = try AVCaptureDeviceInput(device: camera)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        
        if error == nil && session!.canAddInput(input){
            session!.addInput(input)
            do{
            try camera.lockForConfiguration()
            camera.activeVideoMinFrameDuration = CMTimeMake(1, fps)
            camera.activeVideoMaxFrameDuration = CMTimeMake(1, fps)
            camera.unlockForConfiguration()
            } catch {
                print("fps configuration couldn't be locked")
            }
            
            videoDataOutput = AVCaptureVideoDataOutput()
            videoDataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as AnyHashable as! String: NSNumber(value: kCVPixelFormatType_32BGRA)]
            videoDataOutput.alwaysDiscardsLateVideoFrames = true
            
            let queue = DispatchQueue(label: "camerabase64", qos: .background)
            videoDataOutput.setSampleBufferDelegate(self, queue: queue)
            
            if session!.canAddOutput(videoDataOutput) {
                mainCommand = command
                session!.addOutput(videoDataOutput)
                
                guard let connection = videoDataOutput.connection(with:AVMediaType.video) else { return }
                guard connection.isVideoOrientationSupported else { return }
                connection.videoOrientation = .portrait

                hasRun = true;
                session!.startRunning()
            }
        }
    }
    
    @objc(pause:)
    func pause(command: CDVInvokedUrlCommand){
        if session?.isRunning ?? false {
            session?.stopRunning()
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput,  didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        autoreleasepool{
            let  imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
            CVPixelBufferLockBaseAddress(imageBuffer!, CVPixelBufferLockFlags.readOnly)
            let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer!)
            let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer!)
            let width = CVPixelBufferGetWidth(imageBuffer!)
            let height = CVPixelBufferGetHeight(imageBuffer!)
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Little.rawValue
            bitmapInfo |= CGImageAlphaInfo.premultipliedFirst.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
            let context = CGContext.init(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo)
            let quartzImage = context?.makeImage()
            CVPixelBufferUnlockBaseAddress(imageBuffer!, CVPixelBufferLockFlags.readOnly)
            let image = UIImage.init(cgImage: quartzImage!)
            let imageData = UIImageJPEGRepresentation(image, 1)
            let base64 = imageData?.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithLineFeed)
            let javascript = "cordova.iosStream.capture('data:image/jpeg;base64,\(base64!)')"
            
            if let webView = webView {
                if let wkWebView = webView as? WKWebView {
                    DispatchQueue.main.async{
                        wkWebView.evaluateJavaScript(javascript)
                    }
                }
            } else {
                print("webView is nil")
            }
        }
    }
}