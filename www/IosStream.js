var exec = require ('cordova/exec');

exports.startCapture = function(direction,fps){
    exec(null, null, 'IosStream', 'startCapture', [direction,fps]);
};

exports.pause = function () {
    exec(null, null, 'IosStream', 'pause', []);
};

exports.capture = function(data){
    /**
     * placeholder, iOS is going to call this
     * directly when the camera stream is ready.
     * 
     * Example:
     * 
     * cordova.iosStream.capture = function (data) {
     * Create a new image from binary data
     * image.src = data;
     * };
     * //Starting the stream
     * cordova.iosStream.startCapture('back',30);
     */
};